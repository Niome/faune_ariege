<html>
    <head>
        <meta charset="utf-8" />
        <title>Faune Ariégeoise</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
<body>
    <header>
        <h1>Faune Ariégeoise</h1>
    </header>
    <div class="form">
        <form action="animal_post.php" method="post" enctype="multipart/form-data">
            Choisissez une photo
            <input type="file" name="fileToUpload" id="fileToUpload"><br/>
            <label for="Nom">Nom * </label><input type="text" name="Nom" id="Nom" required=""/><br/>
            <label for="Espece">Espece * </label><input type="text" name="Espece" id="Espece" required=""/><br/>
            <label for="Famille">Famille * </label><input type="text" name="Famille" id="Famille" required=""/><br/>
            <label for="Statut">Statut * </label>            
            <select id="Statut" type="text" name="Statut" required="">
            <option>Sauvage</option>
            <option>Domestique</option>
            <option>Semi-domestique</option>
            </select><br/>
            <label for="Nombre">Nombre * </label><input type="number" name="Nombre" min="0" id="Nombre" required=""/><br/>
            <label for="Description">Description * </label><input type="text" name="Description" id="Description" required=""/><br/>
            <button type="submit" value="Envoyer" name="choix">Valider</button>
        </form>
    </div>

<?php
try
{
$bdd = new PDO('mysql:host=localhost;dbname=Faune_Ariege;charset=utf8', 'niome', 'niome', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
}
catch(Exception $e)
{
die('Erreur : '.$e->getMessage());
}
$sql = "SELECT * FROM Animal ORDER BY Nom ASC" ;
$result = $bdd->query($sql);
while ($donnees = $result->fetch(PDO::FETCH_ASSOC))
{
echo(
"<div class=\"animal\">
    <div class=\"fiche\">
            <img id=\"imageajout\" src=\"images/".$donnees['ID']."\">
            <span>Nom : ".$donnees['Nom']."</span> 
            <span>Espece : ".$donnees['Espece']."</span>
            <span>Famille : ".$donnees['Famille']."</span> 
            <span>Statut : ".$donnees['Statut']."</span>
            <span>Nombre : ".$donnees['Nombre']."</span>
            <span>Description : ".$donnees['Description']."</span>
            <a href=\"animal2.php?idNom=".$donnees['ID']."\" id=\"modifier\">Modifier</a>\n
            <a href=\"animal_delete.php?idNom=".$donnees['ID']."\" id=\"supprimer\">Supprimer</a>
    </div>
</div>\n") ;
}
?>
</body>
</html>