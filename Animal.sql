-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 17 Mai 2019 à 16:52
-- Version du serveur :  5.7.26-0ubuntu0.18.04.1
-- Version de PHP :  7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `Faune_Ariege`
--

-- --------------------------------------------------------

--
-- Structure de la table `Animal`
--

CREATE TABLE `Animal` (
  `ID` int(11) NOT NULL,
  `Nom` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Espece` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Famille` varchar(50) NOT NULL,
  `Statut` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Nombre` int(11) NOT NULL,
  `Description` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Animal`
--

INSERT INTO `Animal` (`ID`, `Nom`, `Espece`, `Famille`, `Statut`, `Nombre`, `Description`) VALUES
(26, 'Cerf élaphe', 'Cerf', 'Cervidés', 'Sauvage', 100, 'Il atteint environ 1,20 m au garrot pour un poids entre 60 et 200 kg. Il peut perdre jusqu’à 20 % de son poids durant les périodes de brame. Son pelage est en été rouge sombre et brun-gris en hiver avec un miroir jaunâtre sur les fesses. La ramure (les bois) du mâle, qui tombe à la fin de l’hiver, peut mesurer jusqu’à 1,80 m, les velours seront visibles en mars mais la repousse ne sera complète qu’au bout de 100 jours.'),
(30, 'Vipère aspic', 'Serpent', 'Viperidae', 'Sauvage', 200, 'La Vipère aspic possède un museau retroussé lui conférant un profil anguleux caractéristique (l’autre vipère de notre région, la V. péliade, a plutôt un profil de pantoufle). C’est un serpent de petite taille qui n’atteint pas le mètre.'),
(31, 'Renard roux', 'Renard', 'Canidae', 'Sauvage', 150, 'La couleur du pelage du Renard roux est variable, mais généralement brun-roux (forme foncée appelée « renard charbonnier »). Son museau est étroit et ses lèvres blanches. Il arbore une longue queue touffue, souvent blanche au bout.'),
(32, 'Lynx boréal', 'Lynx', 'Felidae', 'Sauvage', 50, 'Le Lynx boréal est un gros Félidé à la robe unie parsemée de taches ou de rayures. Il a la particularité d’avoir des pinceaux de poils aux oreilles et la queue courte. La couleur de son pelage est très variable, allant du blanc crème, gris-beige, jaune-roux au brun foncé.'),
(33, 'Ecureuil roux', 'Ecureuil', 'Erinaceidae', 'Sauvage', 300, 'L’Ecureuil roux a les oreilles terminées de pinceaux de longs poils, plus développés en hiver ; une queue en panache qui est plus longue chez la femelle. Son pelage est brun rouge (plus fréquent dans les forêts de feuillus, en plaine) à brun sombre (dans les forêts de conifères, en altitude) - présence de noir plus ou moins marqué dans les populations d’altitude, donnant un aspect « charbonnier ».'),
(34, 'Blaireau européen', 'Blaireau', 'Mustélidés', 'Sauvage', 250, 'Le Blaireau européen se reconnaît inévitablement aux bandes noires qui ornent sa tête, du museau à l’arrière des oreilles. Le reste du pelage est grisé sur le dos, composé de poils tricolores (blanc-noir-blanc) et plus franchement brun-noir sur le ventre et les pattes. La queue est blanche.');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Animal`
--
ALTER TABLE `Animal`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Animal`
--
ALTER TABLE `Animal`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
