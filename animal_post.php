<?php 
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=Faune_Ariege;charset=utf8', 'niome', 'niome');
}
catch(Exception $e)
{
        die('MonErreur : '.$e->getMessage());
}
$req = $bdd->prepare('INSERT INTO Animal (Nom, Espece, Famille, Statut, Nombre, Description) VALUES(?, ?,  ?, ?, ?, ?)');
$req->execute(array($_POST['Nom'], $_POST['Espece'], $_POST['Famille'], $_POST['Statut'], $_POST['Nombre'], $_POST['Description']));

$target_dir = "images/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "Le fichier n'est pas une image.";
        $uploadOk = 0;
    }
}
if (file_exists($target_file)) {
    echo "Le fichier existe déjà.";
    $uploadOk = 0;
}
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Le fichier est trop gros.";
    $uploadOk = 0;
}
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Seuls les formats JPG, JPEG, PNG et GIF sont acceptés.";
    $uploadOk = 0;
}
if ($uploadOk == 0) {
    echo "Le fichier n'a pas été chargé.";
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_dir . basename($bdd->lastInsertId()))) {
        echo "Le fichier ". basename( $_FILES["fileToUpload"]["name"]). " a bien été ajouté.";
    } else {
        echo "Il y a eu une erreur lors du chargement du fichier.";
    }
}
header('location: animal.php');
?>